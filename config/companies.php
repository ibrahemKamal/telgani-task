<?php


use App\Services\ExternalCompanies\CompanyHandlers\BaseHandlers\BarCompanyHandler;
use App\Services\ExternalCompanies\CompanyHandlers\BaseHandlers\BazCompanyHandler;
use App\Services\ExternalCompanies\CompanyHandlers\BaseHandlers\FooCompanyHandler;

return [
    'FOO' => [
        'company_handler' => FooCompanyHandler::class,
    ],
    'BAR' => [
        'company_handler' => BarCompanyHandler::class,
    ],
    'BAZ' => [
        'company_handler' => BazCompanyHandler::class,
    ],
];

<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MovieController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)
    ->as('auth.')->group(function () {
        Route::post('login', 'login')->name('login');
    });
Route::get('movies', [MovieController::class, 'index'])->name('movies.titles');


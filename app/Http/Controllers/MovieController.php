<?php

namespace App\Http\Controllers;

use App\Services\ExternalCompanies\ExternalCompanyService;

class MovieController extends Controller
{
    public function index()
    {
        $response = (new ExternalCompanyService())
            ->getMovies();
        return response()->json($response);
    }
}

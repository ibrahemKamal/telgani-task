<?php

namespace App\Http\Controllers;

use App\Services\ExternalCompanies\ExternalCompanyService;
use App\Services\JwtService;

class AuthController extends Controller
{
    private JwtService $jwtService;

    public function __construct(JwtService $jwtService)
    {
        $this->jwtService = $jwtService;
    }

    // I would also add a request to validate the required keys
    public function login()
    {
        $available_companies = array_keys(config('companies'));
        $company_name = str(request('login'))->before('_')->value();

        if (!in_array($company_name, $available_companies)) {
            return $this->fail();
        }

        $attempt = (new ExternalCompanyService)
            ->setCompany($company_name)
            ->authenticate(request('login'), request('password'));

        return $attempt ? $this->success($company_name, request('login')) : $this->fail();
    }

    protected function fail()
    {
        return response()->json(['status' => 'failure'], 422);
    }

    protected function success($company, $username)
    {
        $token = $this->jwtService
            ->setUsername($username)
            ->setClaims(['context' => $company])
            ->handle();
        return response()->json([
            'status' => 'success',
            'token' => $token->toString()
        ]);
    }
}

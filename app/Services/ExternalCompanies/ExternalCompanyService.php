<?php

namespace App\Services\ExternalCompanies;

use App\Services\ExternalCompanies\Contracts\ExternalCompanyInterface;
use App\Services\ExternalCompanies\Exceptions\MissingConfigurationException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;

class ExternalCompanyService
{
    public ExternalCompanyInterface $externalCompany;
    public ?string $companyKey = null;
    // in irl scenario this should be in config file
    private int $cacheTtl = 60; // in seconds

    /**
     * @throws \Throwable
     * @throws MissingConfigurationException
     */
    public function setCompany(string $companyKey): static
    {
        $this->companyKey = $companyKey;
        $this->validateConfiguration();
        $this->externalCompany = $this->getCompanyHandler($companyKey);
        return $this;
    }

    /**
     * @return void
     * @throws MissingConfigurationException
     * @throws \Throwable
     */
    public function validateConfiguration(): void
    {
        // check for company configuration existence
        $configuration = $this->getConfiguration($this->companyKey);
        throw_unless($configuration,
            new MissingConfigurationException("Missing configuration for company: $this->companyKey"));
        // check for required configuration keys
        foreach ($this->getConfigurationKeys() as $key) {
            if (!array_key_exists($key, $configuration)) {
                throw new MissingConfigurationException("Missing configuration key: $key for company: $this->companyKey");
            }
        }
    }

    private function getConfiguration(string $companyKey)
    {
        return config("companies.$companyKey");
    }

    private function getConfigurationKeys(): array
    {
        return [
            'company_handler',
        ];
    }

    private function getCompanyHandler(string $companyKey)
    {
        return app(config("companies.$companyKey.company_handler"));
    }

    public function authenticate(string $userName, string $password): bool
    {
        return $this->externalCompany->getAuthHandler()->authenticate($userName, $password);
    }

    public function getMovies(?string $companyKey = null): array
    {
        $companies = $companyKey ? [$companyKey] : array_keys(config('companies'));
        $movies = [];
        foreach ($companies as $company) {
            $companyCacheKey = $company.'_movies';
            if (Cache::has($companyCacheKey)) {
                $movies[] = Cache::get($companyCacheKey);
                continue;
            }
            $this->setCompany($company);
            $moviesToBeCached = $this->externalCompany->getMoviesHandler()->getMovies();
            if (count($moviesToBeCached) === 0) {
                continue;
            }
            Cache::put($companyCacheKey, $moviesToBeCached, $this->cacheTtl);
            $movies[] = $moviesToBeCached;
        }
        return Arr::flatten($movies);
    }

}

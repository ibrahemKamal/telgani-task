<?php

namespace App\Services\ExternalCompanies\CompanyHandlers\AuthHandlers;

use App\Services\ExternalCompanies\Contracts\CompanyAuthInterface;
use External\Baz\Auth\Authenticator;
use External\Baz\Auth\Responses\Success;

class BazAuthHandler implements CompanyAuthInterface
{

    public function authenticate(string $userName, string $password): bool
    {
        $loginResponse = app(Authenticator::class)
            ->auth($userName, $password);
        return $loginResponse instanceof Success;
    }

}

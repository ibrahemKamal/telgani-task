<?php

namespace App\Services\ExternalCompanies\CompanyHandlers\AuthHandlers;

use App\Services\ExternalCompanies\Contracts\CompanyAuthInterface;
use External\Foo\Auth\AuthWS;

class FooAuthHandler implements CompanyAuthInterface
{

    public function authenticate(string $userName, string $password): bool
    {
        try {
            app(AuthWS::class)
                ->authenticate($userName, $password);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

}

<?php

namespace App\Services\ExternalCompanies\CompanyHandlers\AuthHandlers;

use App\Services\ExternalCompanies\Contracts\CompanyAuthInterface;
use External\Bar\Auth\LoginService;

class BarAuthHandler implements CompanyAuthInterface
{

    public function authenticate(string $userName, string $password): bool
    {
        return app(LoginService::class)
            ->login($userName, $password);
    }

}

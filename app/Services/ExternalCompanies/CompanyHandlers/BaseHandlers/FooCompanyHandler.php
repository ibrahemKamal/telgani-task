<?php

namespace App\Services\ExternalCompanies\CompanyHandlers\BaseHandlers;

use App\Services\ExternalCompanies\CompanyHandlers\AuthHandlers\FooAuthHandler;
use App\Services\ExternalCompanies\CompanyHandlers\MoviesHandlers\FooMoviesHandler;
use App\Services\ExternalCompanies\Contracts\CompanyAuthInterface;
use App\Services\ExternalCompanies\Contracts\CompanyMoviesInterface;
use App\Services\ExternalCompanies\Contracts\ExternalCompanyInterface;

class FooCompanyHandler implements ExternalCompanyInterface
{

    public function getAuthHandler(): CompanyAuthInterface
    {
        return new FooAuthHandler;
    }

    public function getMoviesHandler(): CompanyMoviesInterface
    {
        return new FooMoviesHandler;
    }
}

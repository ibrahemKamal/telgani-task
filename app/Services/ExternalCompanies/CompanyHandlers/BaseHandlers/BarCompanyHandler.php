<?php

namespace App\Services\ExternalCompanies\CompanyHandlers\BaseHandlers;

use App\Services\ExternalCompanies\CompanyHandlers\AuthHandlers\BarAuthHandler;
use App\Services\ExternalCompanies\CompanyHandlers\MoviesHandlers\BarMoviesHandler;
use App\Services\ExternalCompanies\Contracts\CompanyAuthInterface;
use App\Services\ExternalCompanies\Contracts\CompanyMoviesInterface;
use App\Services\ExternalCompanies\Contracts\ExternalCompanyInterface;

class BarCompanyHandler implements ExternalCompanyInterface
{

    public function getAuthHandler(): CompanyAuthInterface
    {
        return new BarAuthHandler;
    }

    public function getMoviesHandler(): CompanyMoviesInterface
    {
        return new BarMoviesHandler;
    }
}

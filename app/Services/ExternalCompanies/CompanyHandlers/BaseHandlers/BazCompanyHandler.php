<?php

namespace App\Services\ExternalCompanies\CompanyHandlers\BaseHandlers;

use App\Services\ExternalCompanies\CompanyHandlers\AuthHandlers\BazAuthHandler;
use App\Services\ExternalCompanies\CompanyHandlers\MoviesHandlers\BazMoviesHandler;
use App\Services\ExternalCompanies\Contracts\CompanyAuthInterface;
use App\Services\ExternalCompanies\Contracts\CompanyMoviesInterface;
use App\Services\ExternalCompanies\Contracts\ExternalCompanyInterface;

class BazCompanyHandler implements ExternalCompanyInterface
{

    public function getAuthHandler(): CompanyAuthInterface
    {
        return new BazAuthHandler;
    }

    public function getMoviesHandler(): CompanyMoviesInterface
    {
        return new BazMoviesHandler;
    }
}

<?php

namespace App\Services\ExternalCompanies\CompanyHandlers\MoviesHandlers;

use App\Services\ExternalCompanies\Contracts\CompanyMoviesInterface;
use External\Baz\Movies\MovieService;

class BazMoviesHandler implements CompanyMoviesInterface
{

    public function getMovies(): array
    {
        try {
            $response = app(MovieService::class)
                ->getTitles();
            return $response['titles'];
        } catch (\Throwable $e) {
            return [];
        }
    }
}

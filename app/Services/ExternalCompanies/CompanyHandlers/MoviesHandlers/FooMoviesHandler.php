<?php

namespace App\Services\ExternalCompanies\CompanyHandlers\MoviesHandlers;

use App\Services\ExternalCompanies\Contracts\CompanyMoviesInterface;
use External\Foo\Movies\MovieService;

class FooMoviesHandler implements CompanyMoviesInterface
{

    public function getMovies(): array
    {
        try {
            return app(MovieService::class)
                ->getTitles();
        } catch (\Throwable $exception) {
            return [];
        }
    }
}

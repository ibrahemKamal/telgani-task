<?php

namespace App\Services\ExternalCompanies\CompanyHandlers\MoviesHandlers;

use App\Services\ExternalCompanies\Contracts\CompanyMoviesInterface;
use External\Bar\Movies\MovieService;

class BarMoviesHandler implements CompanyMoviesInterface
{

    public function getMovies(): array
    {
        try {
            $response = app(MovieService::class)
                ->getTitles();
            return collect($response['titles'])->pluck('title')->toArray();
        } catch (\Throwable $e) {
            return [];
        }
    }
}

<?php

namespace App\Services\ExternalCompanies\Contracts;

interface CompanyMoviesInterface
{
    public function getMovies(): array;
}

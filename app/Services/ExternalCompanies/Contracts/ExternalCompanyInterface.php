<?php

namespace App\Services\ExternalCompanies\Contracts;

interface ExternalCompanyInterface
{

    public function getAuthHandler(): CompanyAuthInterface;

    public function getMoviesHandler(): CompanyMoviesInterface;
}

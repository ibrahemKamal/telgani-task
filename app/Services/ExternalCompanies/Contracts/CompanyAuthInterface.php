<?php

namespace App\Services\ExternalCompanies\Contracts;

interface CompanyAuthInterface
{
    public function authenticate(string $userName, string $password): bool;
}

<?php

namespace App\Services;

use Carbon\CarbonImmutable;
use DateTimeImmutable;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Encoding\UnixTimestampDates;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token\Plain;

class JwtService
{
    private array $claims = [];

    private $user_name = null;

    /**
     * @var \DateTimeImmutable|null
     */
    private $expiresAt;


    /**
     * Set the claims to include in this JWT.
     *
     * @return $this
     */
    public function setClaims(array $claims): static
    {
        $this->claims = $claims;

        return $this;
    }

    /**
     * Attaches a user to the JWT being created and will automatically inject the
     * "user_uuid" key into the final claims array with the user's UUID.
     */
    public function setUsername($user_name): self
    {
        $this->user_name = $user_name;

        return $this;
    }

    /**
     * @return $this
     */
    public function setExpiresAt(DateTimeImmutable $date): static
    {
        $this->expiresAt = $date;

        return $this;
    }


    /**
     * Generate a new JWT for a given node.
     *
     * @param  string|null  $identifiedBy
     *
     * @return \Lcobucci\JWT\Token\Plain
     */
    public function handle(): Plain
    {
        // in real app i'd create a separate env_key for this key instead if using laravel's app key
        $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText(config('app.key')));

        $builder = $config->builder(new UnixTimestampDates())
            ->issuedAt(CarbonImmutable::now());

        if ($this->expiresAt) {
            $builder = $builder->expiresAt($this->expiresAt);
        }

        foreach ($this->claims as $key => $value) {
            $builder = $builder->withClaim($key, $value);
        }

        if (!is_null($this->user_name)) {
            $builder = $builder
                ->withClaim('login', $this->user_name);
        }

        return $builder
            ->getToken($config->signer(), $config->signingKey());
    }

}
